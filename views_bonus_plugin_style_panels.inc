<?php
/**
 * @file
 * Plugin include file for a panels based style plugin.
 */

/**
 * Multi-column panels rendered output.
 *
 * @ingroup views_style_plugins
 */
class views_bonus_plugin_style_panels extends views_plugin_style {
  /**
   * Set default options
   */
  function options(&$options) {
    parent::options($options);
  }

  /**
   * Use options_form from our parent.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }
}
