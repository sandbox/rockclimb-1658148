<?php
/**
 * @file
 * Views include file with views hooks.
 */

/**
 * Implements hook_views_plugins().
 */
function views_bonus_panels_views_plugins() {
  return array(
    'style' => array(
      'panels_threecol' => array(
        'title' => t('Panels: 3 columns'),
        'help' => t('Display views items devided into 3 columns.'),
        'handler' => 'views_bonus_plugin_style_panels',
        'theme' => 'views_bonus_panels_render',
        'uses row plugin' => TRUE,
        'type' => 'normal',
      ),
      'panels_threecol_stack' => array(
        'title' => t('Panels: 1 top + 3 columns'),
        'help' => t('Display views items devided into 3 columns with the first item above.'),
        'handler' => 'views_bonus_plugin_style_panels',
        'theme' => 'views_bonus_panels_render',
        'uses row plugin' => TRUE,
        'type' => 'normal',
      ),
      'panels_twocol' => array(
        'title' => t('Panels: 2 columns'),
        'help' => t('Display views items devided into 2 columns.'),
        'handler' => 'views_bonus_plugin_style_panels',
        'theme' => 'views_bonus_panels_render',
        'uses row plugin' => TRUE,
        'type' => 'normal',
      ),
      'panels_twocol_stack' => array(
        'title' => t('Panels: 1 top + 2 columns'),
        'help' => t('Display views items devided into 2 columns with the first item above.'),
        'handler' => 'views_bonus_plugin_style_panels',
        'theme' => 'views_bonus_panels_render',
        'uses row plugin' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
