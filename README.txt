
This module used to be part of the views_bonus group of modules, but has
been updated for Drupal 7 and split into its own project. It contains 
extra layouts for the views module. The layouts are based on those in the 
panels module.
